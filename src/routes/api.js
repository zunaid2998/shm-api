const express = require("express");
const router = express.Router();

const common_controller = require("./../controllers/commonController");

const userController = require("./../controllers/userController");
const materialController = require("./../controllers/materialController");
const orderController = require("./../controllers/orderController");
const eventController = require("./../controllers/eventController");
const settingsController = require("./../controllers/settingsController");

/*
=============================================================================
Common Routes
=============================================================================
*/
router.post("/auth/login", common_controller.login);

/*
=============================================================================
Material Routes
=============================================================================
*/

router.post("/material", materialController.createMaterial);
router.get("/material/:id", materialController.getMaterial);
router.post("/material/:id", materialController.updateMaterial);
router.get("/material", materialController.findAll);
router.get("/active/material", materialController.findAllActiveMaterials);

/*
=============================================================================
User Routes
=============================================================================
*/

router.post("/user", userController.createUser);
router.get("/user/:id", userController.getUser);
router.post("/user/:id", userController.updateUser);
router.get("/user", userController.findAll);
router.get("/user/:id/token", userController.generateApiToken);
router.post("/auth/sendResetPasswordLink", userController.sendResetPasswordLink);
router.post("/auth/checkVerificationCode", userController.checkVerificationCode);

/*
=============================================================================
Order Routes
=============================================================================
*/

router.post("/order", orderController.createOrder);
router.get("/order/:id", orderController.getOrder);
router.post("/order/:id", orderController.updateOrder);
router.get("/order", orderController.findAll);
router.get("/user/:id/order", orderController.findOrdersByCustomer);
router.get("/admin/order/unextracted", orderController.findAllUnextractedOrders);
router.post("/admin/order/:orderNo", orderController.getOrderCsv);
router.get("/order/:orderNo/download", orderController.downloadOrder);
router.delete("/order/:id", orderController.deleteOrder);

router.post("/admin/startDailyReportCronJob", orderController.startDailyReportCronJob)
router.post("/admin/dailyReport", orderController.dailyReport)

/*
=============================================================================
Event Routes
=============================================================================
*/

router.post("/event", eventController.createEvent);
router.get("/event", eventController.findAll);

/*
=============================================================================
Settings Routes
=============================================================================
*/

router.post("/settings", settingsController.createSettings);
router.get("/settings/:id", settingsController.getSettings);
router.post("/settings/:id", settingsController.updateSettings);


module.exports = router;

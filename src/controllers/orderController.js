

const OrderModel = require("../models/orderModel");
const SettingsModel = require("../models/settingsModel");
const moment = require("moment")
const { handleError } = require("../utils/errorHandler");
const { convertArrayToCSV } = require('convert-array-to-csv');
const { sendOrderCreationEmail, sendOrderCreationEmailToCustomer, 
  sendOrderProcessedEmailToCustomer, sendDailyReport } = require("./../utils/email");
const converter = require('convert-array-to-csv');
const _ = require("lodash");
const json2csv = require('json2csv').parse;
const CronJob = require('cron').CronJob;

const fs = require("fs")

/*
=============================================================================
Get existing order
=============================================================================
*/
exports.getOrder = async function (req, res) {
  try {
    if (!OrderModel.isValidId(req.params.id)) {
      throw new Error("Supplied id is invalid");
    }

    let order = await OrderModel.findById(req.params.id).exec();
    res.send({
      data: order,
      msg: order ? "order found" : "order not found"
    });

  } catch (err) {
    handleError(err, res);
  }
};


/*
=============================================================================
Create a  Order
=============================================================================
*/

exports.createOrder = async function (req, res) {
  try {

    const orders = await OrderModel.find().sort({createdAt: -1}).exec();
    if(orders.length === 0){
      req.body.orderNo = 1000
    } else {
      const lastOrderNo = orders[0].orderNo
      const orderNo = lastOrderNo ? lastOrderNo + 1 : 1000
      
      const order = await OrderModel.findOne({orderNo}).exec();
      if (order) {
        res.send({
          data: req.body,
          message: "Something went wrong while creating this order! Please try again."
        });
        return;
      }
      
      req.body.orderNo = orderNo
    }
    
    const order = new OrderModel(req.body);

    await order.save();
    
    res.send({
      data: order,
      message: "Order added successfully."
    });

    // send email about the order
    let settings = await SettingsModel.find().exec();
    if(settings && settings.length > 0) {
      sendOrderCreationEmail(settings[0].sendOrderToEmail, order);
    }
    sendOrderCreationEmailToCustomer(order)
    
    // create a csv file against this order
    const materials = _.clone(order.materials)
    generateCsv(materials, order.email)
    

  } catch (error) {
    handleError(error, res);
  }
};

const generateCsv = (data, customerEmail) => {
  data.map(item => {
    delete item.id
    delete item.checked
    return item
  })
  const header = ['number', 'description', 'quantity', 'unitOfMeasure'];

  const materialsCSV = convertArrayToCSV(data, {
    header,
    separator: '|'
  })
  const date = new Date()
  const today = date.getMonth() + 1 + '-' + date.getDate() + '-' + date.getFullYear()
  fs.writeFileSync(`uploads/order/${customerEmail}_${today}.csv`, materialsCSV, () => {
    console.log("CSV done")
  })
}


/*
=============================================================================
Udpdate  an existing Order
=============================================================================
*/
const opts = { runValidators: true, useFindAndModify: false, new: true };
exports.updateOrder = async function (req, res) {
  try {
    if (!OrderModel.isValidId(req.params.id)) {
      throw new Error("Supplied order id is invalid");
    }

    let updatedItem = await OrderModel.findByIdAndUpdate(
      req.params.id,
      { $set: req.body },
      opts
    ).exec();

    res.send({
      data: updatedItem,
      message: updatedItem
        ? `Order updated successfully`
        : `Order failed to updated`
    });

    if(updatedItem.status === 'processed'){
      sendOrderProcessedEmailToCustomer(updatedItem)
    }
  } catch (err) {
    handleError(err, res);
  }
};


/*
=============================================================================
Hard Delete  an existing Order
=============================================================================
*/

exports.deleteOrder = async function (req, res) {
  try {
    if (!OrderModel.isValidId(req.params.id)) {
      throw new Error("Supplied order id is invalid");
    }

    await OrderModel.findOneAndDelete({_id: req.params.id}).exec();

    res.send({
      data: null,
      message: 'Order Deleted'
    });
    
  } catch (err) {
    handleError(err, res);
  }
};

/*
 *api/orders: get all orders
 */
exports.findAll = async function (req, res) {
  try {
    let orders = null;

    orders = await OrderModel.find().sort({createdAt: -1}).exec();

    res.send({
      data: orders,
      message: ``
    });
  } catch (err) {
    handleError(err, res);
  }
};

/*
 *api/user/:id/order: get all orders
 */
exports.findOrdersByCustomer = async function (req, res) {
  try {
    let orders = null;

    orders = await OrderModel.find({customerId: req.params.id}).sort({createdAt: -1}).exec();

    res.send({
      data: orders,
      message: ``
    });
  } catch (err) {
    handleError(err, res);
  }
};

// Reporting specific orders

/**
 * api/admin/order/unextracted?token={token}
 */

exports.findAllUnextractedOrders = async function (req, res) {
  try {
    const token = req.query.token 
    if(!token) {
      res.send({ data: null, message: `Valid token required`})
      return
    }

    let user = await UserModel.findOne({apiToken: token, userType: 'admin'}).exec();
    if(!user) {
      res.send({ data: null, message: `Valid token required`})
      return
    }

    let unextractedOrders = await OrderModel.find({extracted: false}).exec();
    let ordersNotWithExtractedFields = await OrderModel.find({extracted: null}).exec()
    let allOrders = unextractedOrders.concat(ordersNotWithExtractedFields)
    let ordersWithOrderNo = allOrders.filter(order => order.orderNo !== undefined)
    let orders = ordersWithOrderNo.map(order => {
      const orderCreatedAt = new Date(order.createdAt)
      const fullYear = orderCreatedAt.getFullYear()
      const month = ((orderCreatedAt.getMonth() + 1) < 10 ? '0' : '') + (orderCreatedAt.getMonth() + 1)
      const day = (orderCreatedAt.getDate() < 10 ? '0' : '') + orderCreatedAt.getDate();
      const hour = (orderCreatedAt.getHours() < 10 ? '0' : '') + orderCreatedAt.getHours();
      const minute = (orderCreatedAt.getMinutes() < 10 ? '0' : '') + orderCreatedAt.getMinutes();
      const second = (orderCreatedAt.getSeconds() < 10 ? '0' : '') + orderCreatedAt.getSeconds();
      const fileName = `${order.orderNo}_${fullYear}${month}${day}_${hour}${minute}${second}`
      return fileName
    })
  
    let orderNumbers = orders.join("\n")

    res.send(orderNumbers);
  } catch (err) {
    handleError(err, res);
  }
};

/**
 * api/admin/order/:orderNo?token={token}
 */

exports.getOrderCsv = async function (req, res) {
  try {
    const token = req.query.token
    if(!token) {
      res.send({ data: null, message: `Valid token required`})
      return
    }

    let user = await UserModel.findOne({apiToken: token, userType: 'admin'}).exec();
    if(!user) {
      res.send({ data: null, message: `Valid token required`})
      return
    }

    const orderWithDate = req.params.orderNo
    const orderNumber = orderWithDate.split('_')[0]
    let order = await OrderModel.findOne({orderNo: orderNumber}).exec();
    if(!order) {
      res.send({ data: null, message: `Invalid order number`})
      return
    }

    orders = []
    const {orderNo, address, orderPriority, deliveryInstructions, createdAt} = order
    order.materials.map(material => {
      const {number, description, quantity, unitOfMeasure} = material
      orders.push({
        orderNo: orderNo,
        materialNo: number,
        description: description,
        quantity: quantity,
        unitOfMeasure: unitOfMeasure,
        facilityName: address.addressName,
        address: address.addressLine1,
        city: address.city,
        postalCode: getPostalCode(address.postalCode),
        orderPriority: orderPriority,
        deliveryInstructions: getDeliveryInstructions(deliveryInstructions)
      })
    })

    const fields = ['orderNo', 'materialNo', 'description', 'quantity', 'unitOfMeasure', 
              'facilityName', 'address', 'city', 'postalCode', 'orderPriority', 'deliveryInstructions'];
    const data = json2csv(orders, {delimiter: '|', header: false, quote: ''});

    const orderCreatedAt = new Date(createdAt)
    const fullYear = orderCreatedAt.getFullYear()
    const month = ((orderCreatedAt.getMonth() + 1) < 10 ? '0' : '') + (orderCreatedAt.getMonth() + 1)
    const day = (orderCreatedAt.getDate() < 10 ? '0' : '') + orderCreatedAt.getDate();
    const hour = (orderCreatedAt.getHours() < 10 ? '0' : '') + orderCreatedAt.getHours();
    const minute = (orderCreatedAt.getMinutes() < 10 ? '0' : '') + orderCreatedAt.getMinutes();
    const second = (orderCreatedAt.getSeconds() < 10 ? '0' : '') + orderCreatedAt.getSeconds();
    const fileName = `${order.orderNo}_${fullYear}${month}${day}_${hour}${minute}${second}`
    res.attachment(`${fileName}.txt`)
    
    if(req.body.extracted !== undefined) {
      await OrderModel.findByIdAndUpdate(
        order._id,
        { $set: {extracted: req.body.extracted} },
        opts
      ).exec();
    }
    res.status(200).send(data)
  } catch (err) {
    handleError(err, res);
  }
};

const getPostalCode = (postalCode) => {
  if(postalCode && postalCode.length === 6) {
    const first = postalCode.substring(0, 3)
    const last = postalCode.substring(3, 6)
    return `${first} ${last}`
  }
  return postalCode
}

const getDeliveryInstructions = (deliveryInstructions) => {
  if(!deliveryInstructions) {
    return ''
  }
  return deliveryInstructions.replace(/\r?\n|\r/g, " ")
}

/**
 * api/order/:orderNo/download
 */

exports.downloadOrder = async function (req, res) {
  try {

    let order = await OrderModel.findOne({orderNo: req.params.orderNo}).exec();
    if(!order) {
      res.send({ data: null, message: `Invalid order number`})
      return
    }

    orders = []
    const {orderNo, address, orderPriority, deliveryInstructions, createdAt} = order
    order.materials.map(material => {
      const {number, description, quantity, unitOfMeasure} = material
      orders.push({
        orderNo: orderNo,
        materialNo: number,
        description: description,
        quantity: quantity,
        unitOfMeasure: unitOfMeasure,
        facilityName: address.addressName,
        address: address.addressLine1,
        city: address.city,
        postalCode: getPostalCode(address.postalCode),
        orderPriority: orderPriority,
        deliveryInstructions: getDeliveryInstructions(deliveryInstructions)
      })
    })

    res.status(200).send({
      data: {
        orders,
        order,
        createdAt
      },
    })
  } catch (err) {
    handleError(err, res);
  }
};

exports.startDailyReportCronJob = async function (req, res) {
  const emails = req.body.emails
  const job = new CronJob('0 0 * * *', async () => {
    console.log('cron job started with every midnight to generate a report');
    exportAndSendLastDaysOrder(emails, res)
  });
  
  job.start();
  res.status(200).send({
    message: "Cron job started"
  })
}


/**
 * api/admin/order/exportAndSendLastDaysOrder
 */

const exportAndSendLastDaysOrder = async function (emails, res) {
  try {
    const endTime = new Date().getTime();
    const startTime = new Date(endTime - (24*60*60*1000)).getTime();
    let allOrders = await OrderModel.find({}).exec();
    
    filteredOrders = []
    await allOrders.map(async order => {
      const createdAtTime = new Date(order.createdAt).getTime()
      if(createdAtTime >= startTime && createdAtTime <= endTime ){
        filteredOrders.push(order)
      }
    })

    if(filteredOrders.length > 0) {
      orders = []
      await filteredOrders.map(async order => {
        const {orderNo, address, orderPriority, deliveryInstructions, createdAt} = order
        order.materials.map(async material => {
          const {number, description, quantity, unitOfMeasure} = material
          orders.push({
            orderNo: orderNo,
            materialNo: number,
            description: description,
            quantity: quantity,
            unitOfMeasure: unitOfMeasure,
            facilityName: address.addressName,
            address: address.addressLine1,
            city: address.city,
            postalCode: getPostalCode(address.postalCode),
            orderPriority: orderPriority,
            deliveryInstructions: getDeliveryInstructions(deliveryInstructions)
          })
        })
      })

      const data = json2csv(orders, {delimiter: '|', header: false, quote: ''});
      
      const orderDate = new Date(startTime);
      const fullYear = orderDate.getFullYear()
      const month = ((orderDate.getMonth() + 1) < 10 ? '0' : '') + (orderDate.getMonth() + 1)
      const day = (orderDate.getDate() < 10 ? '0' : '') + orderDate.getDate();
      const fileName = `dailyreport-${fullYear}${month}${day}`
      await fs.writeFileSync(`uploads/dailyreport/${fileName}.txt`, data, () => {})
      sendDailyReport(emails, fileName)
      res.status(200).send()
    } else {
      console.log('No orders found')
      res.status(200).send({
        message: "No orders found"
      })
    }
   } catch (err) {
    handleError(err, res);
   }
}


/**
 * api/admin/dailyReport
 */

 exports.dailyReport = async function (req, res) {
  const emails = req.body.emails
  const queryDate = req.body.date
  
  try {
    let allOrders = await OrderModel.find({}).exec();
    
    filteredOrders = []
    await allOrders.map(async order => {
      const createdAt = new Date(order.createdAt)
      const fullYear = createdAt.getFullYear()
      const month = ((createdAt.getMonth() + 1) < 10 ? '0' : '') + (createdAt.getMonth() + 1)
      const day = (createdAt.getDate() < 10 ? '0' : '') + createdAt.getDate();
      const date = `${fullYear}-${month}-${day}`
      if(date === queryDate) {
        filteredOrders.push(order)
      }
    })

    if(filteredOrders.length > 0) {
      orders = []
      await filteredOrders.map(async order => {
        const {orderNo, address, orderPriority, deliveryInstructions, createdAt} = order
        order.materials.map(async material => {
          const {number, description, quantity, unitOfMeasure} = material
          orders.push({
            orderNo: orderNo,
            materialNo: number,
            description: description,
            quantity: quantity,
            unitOfMeasure: unitOfMeasure,
            facilityName: address.addressName,
            address: address.addressLine1,
            city: address.city,
            postalCode: getPostalCode(address.postalCode),
            orderPriority: orderPriority,
            deliveryInstructions: getDeliveryInstructions(deliveryInstructions)
          })
        })
      })

      const data = json2csv(orders, {delimiter: '|', header: false, quote: ''});
      const fileName = `dailyreport-${queryDate}`
      await fs.writeFileSync(`uploads/dailyreport/${fileName}.txt`, data, () => {})
      sendDailyReport(emails, fileName)
      res.status(200).send()
    } else {
      console.log('No orders found')
      res.status(200).send({
        message: "No orders found"
      })
    }
   } catch (err) {
    handleError(err, res);
   }
}
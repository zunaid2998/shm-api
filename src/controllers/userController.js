

const UserModel = require("../models/userModel");
const { handleError } = require("../utils/errorHandler");
const bcrypt = require("bcryptjs");
const moment = require("moment")
const { sendResetPasswordLink } = require("./../utils/email");
const { v4: uuidv4 } = require("uuid");
const TokenGenerator = require('uuid-token-generator');

/*
=============================================================================
Get existing User
=============================================================================
*/
exports.getUser = async function(req, res) {
  try {
    if (!UserModel.isValidId(req.params.id)) {
      throw new Error("Supplied id is invalid");
    }

    let user = await UserModel.findById(req.params.id).exec();
    res.send({
      data : user,
      msg : user ? "user found" : "user not found"
    });
    
  } catch (err) {
    handleError(err, res);
  }
};
 

/*
=============================================================================
Create a  User
=============================================================================
*/

exports.createUser = async function(req, res) {
  try {
    if(req.body.password){
      req.body.password = await bcrypt.hash(req.body.password, 10)
    }
    const user = new UserModel(req.body);

    await user.save();

    res.send({
      data: user,
      message: "User added successfully."
    });
  } catch (error) {
    res.send({
      data: null,
      message: "An user with this email already exists"
    });
  }
};


/*
=============================================================================
Udpdate  an existing User
=============================================================================
*/
const opts = { runValidators: true, useFindAndModify: false , new : true};
exports.updateUser = async function(req, res) {
  try {
    if (!UserModel.isValidId(req.params.id)) {
      throw new Error("Supplied user id is invalid");
    }

    if(req.body.password){
      req.body.password = await bcrypt.hash(req.body.password, 10)
    }

    let updatedItem = await UserModel.findByIdAndUpdate(
      req.params.id,
      { $set: req.body },
      opts
    ).exec();

    res.send({
      data: updatedItem,
      message: updatedItem
        ? `User updated successfully`
        : `User failed to updated`
    });
  } catch (err) {
    handleError(err, res);
  }
};

/*
 *api/users: get all materials
 */
exports.findAll = async function(req, res) {
  try {
    let users = null;

    users = await UserModel.find().exec();

    res.send({
      data: users,
      message: ``
    });
  } catch (err) {
    handleError(err, res);
  }
};

/*
 *api/sendResetPasswordLink: Send reset password link
 */
exports.sendResetPasswordLink = async function(req, res) {
  try {
    const email = req.body.email
    let user = await UserModel.findOne({email});
    if(!user) {
      res.send({
        data: null,
        message: 'Invalid Email'
      })
    }

    user.emailVerification.code = uuidv4()
    user.emailVerification.expiredAt = moment().add(1, 'day')
    console.log(user.emailVerification.expiredAt)
    await user.save()

    // send reset password link
    sendResetPasswordLink(user)

    res.send({
      data: user,
      message: 'Reset password email sent'
    });
  } catch (err) {
    handleError(err, res);
  }
};


/*
 *api/sendResetPasswordLink: Send reset password link
 */
exports.checkVerificationCode = async function(req, res) {
  try {
    const code = req.body.code
    let user = await UserModel.findOne({'emailVerification.code': code});
    if(!user) {
      res.send({
        data: null,
        message: 'Invalid Verification Code'
      })
    }

    if(user.emailVerification){
      const currentTime = moment()
      if(currentTime > user.emailVerification.expiredAt) {
        res.send({
          data: null,
          message: 'Verification Code Expired'
        })
      }
    }

    res.send({
      data: user,
      message: 'Verificaiton code passed'
    });
  } catch (err) {
    handleError(err, res);
  }
};


/**
 * Generate api token and save it
 */
exports.generateApiToken = async function(req, res) {
  try {
    if (!UserModel.isValidId(req.params.id)) {
      throw new Error("Supplied user id is invalid");
    }

    const apiToken = new TokenGenerator(256, TokenGenerator.BASE62).generate();

    let updatedItem = await UserModel.findByIdAndUpdate(
      req.params.id,
      { $set: { apiToken } },
      opts
    ).exec();

    res.send({
      data: updatedItem,
      message: updatedItem
        ? `User updated successfully`
        : `User failed to updated`
    });
  } catch (err) {
    handleError(err, res);
  }
}
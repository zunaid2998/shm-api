

const SettingsModel = require("../models/settingsModel");
const { handleError } = require("../utils/errorHandler");
const _ = require("lodash")

/*
=============================================================================
Get existing Settings
=============================================================================
*/
exports.getSettings = async function (req, res) {
  try {
    if (!SettingsModel.isValidId(req.params.id)) {
      throw new Error("Supplied id is invalid");
    }

    let settings = await SettingsModel.findById(req.params.id).exec();
    res.send({
      data: settings,
      msg: settings ? "Settings found" : "Settings not found"
    });

  } catch (err) {
    handleError(err, res);
  }
};


/*
=============================================================================
Create a  Settings
=============================================================================
*/

exports.createSettings = async function (req, res) {
  try {

    const settings = new SettingsModel(req.body);

    await settings.save();
    
    res.send({
      data: settings,
      message: "Settings added successfully."
    });

  } catch (error) {
    handleError(error, res);
  }
};


/*
=============================================================================
Udpdate  an existing Settings
=============================================================================
*/
const opts = { runValidators: true, useFindAndModify: false, new: true };
exports.updateSettings = async function (req, res) {
  try {
    if (!SettingsModel.isValidId(req.params.id)) {
      throw new Error("Supplied Settings id is invalid");
    }

    let updatedItem = await SettingsModel.findByIdAndUpdate(
      req.params.id,
      { $set: req.body },
      opts
    ).exec();

    res.send({
      data: updatedItem,
      message: updatedItem
        ? `Settings updated successfully`
        : `Settings failed to update`
    });
  } catch (err) {
    handleError(err, res);
  }
};

/*
 *api/Settingss: get all Settingss
 */
exports.findAll = async function (req, res) {
  try {
    let settings = null;

    settings = await SettingsModel.find().exec();

    res.send({
      data: settings,
      message: ``
    });
  } catch (err) {
    handleError(err, res);
  }
};

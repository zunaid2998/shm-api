

const EventModel = require("../models/eventModel");
const { handleError } = require("../utils/errorHandler");
const _ = require("lodash")

/*
=============================================================================
Get existing event
=============================================================================
*/
exports.getEvent = async function (req, res) {
  try {
    if (!EventModel.isValidId(req.params.id)) {
      throw new Error("Supplied id is invalid");
    }

    let event = await EventModel.findById(req.params.id).exec();
    res.send({
      data: event,
      msg: event ? "event found" : "event not found"
    });

  } catch (err) {
    handleError(err, res);
  }
};


/*
=============================================================================
Create a  event
=============================================================================
*/

exports.createEvent = async function (req, res) {
  try {

    const event = new EventModel(req.body);

    await event.save();
    
    res.send({
      data: event,
      message: "event added successfully."
    });

  } catch (error) {
    handleError(error, res);
  }
};


/*
=============================================================================
Udpdate  an existing event
=============================================================================
*/
const opts = { runValidators: true, useFindAndModify: false, new: true };
exports.updateEvent = async function (req, res) {
  try {
    if (!EventModel.isValidId(req.params.id)) {
      throw new Error("Supplied event id is invalid");
    }

    let updatedItem = await EventModel.findByIdAndUpdate(
      req.params.id,
      { $set: req.body },
      opts
    ).exec();

    res.send({
      data: updatedItem,
      message: updatedItem
        ? `event updated successfully`
        : `event failed to update`
    });
  } catch (err) {
    handleError(err, res);
  }
};

/*
 *api/events: get all events
 */
exports.findAll = async function (req, res) {
  try {
    let events = null;

    events = await EventModel.find().sort({createdAt: -1}).exec();

    res.send({
      data: events,
      message: ``
    });
  } catch (err) {
    handleError(err, res);
  }
};

/*
=============================================================================
=============================================================================
=============================================================================
=====                                                                     ===
===== All Controllers that are common between client and service provider === 
=====                                                                     ===
=============================================================================
=============================================================================
=============================================================================
*/
const { v4: uuidv4 } = require("uuid");

const UserModel = require("./../models/userModel");

const { handleError } = require("./../utils/errorHandler");
const bcrypt = require("bcryptjs");


/**
 * This function checks for login
 */
exports.login = async function (req, res) {
  try {
    let { email, password } = req.body
   
    //Search user collection first
    let existingUser = await UserModel.findOne({ email });

    //If user is still empty throw error
    if(!existingUser){
      res.json({ data: null, msg: "Email invalid" });
    }

    if(!existingUser.password) {
      res.send({ 
        data: null, 
        msg: "Password not set yet" 
      })
      return
    }

    const isPasswordMatch = await bcrypt.compare(password, existingUser.password);

    //Else carry on and check password
    if (!isPasswordMatch) {
      res.json({ data: null, msg: "Password invalid" });
    }

    if (existingUser.suspended) {
      res.json({ data: "suspended", msg: "Your account has been suspended. Please contact Administrator." });
    }

    res.json({ data: existingUser , msg: "Successfully logged in." });
  } catch (error) {
    handleError(error, res);
  }
};

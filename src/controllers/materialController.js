

const MaterialModel = require("../models/materialModel");
const { handleError } = require("../utils/errorHandler");

/*
=============================================================================
Get existing Job Posting
=============================================================================
*/
exports.getMaterial = async function(req, res) {
  try {
    if (!MaterialModel.isValidId(req.params.id)) {
      throw new Error("Supplied id is invalid");
    }

    let material = await MaterialModel.findById(req.params.id).exec();
    res.send({
      data : material,
      msg : material ? "material found" : "Job not found"
    });
    
  } catch (err) {
    handleError(err, res);
  }
};
 

/*
=============================================================================
Create a  Material
=============================================================================
*/

exports.createMaterial = async function(req, res) {
  try {

    const existingMaterial = await MaterialModel.findOne({number: req.body.number}).exec();
    if(existingMaterial){
      res.status(400).json({ codeName: 'DuplicateKey' });
      return;
    }
    const material = new MaterialModel(req.body);

    let newMaterial = await material.save();

    res.send({
      data: newMaterial,
      message: "Job Posting added successfully."
    });
  } catch (err) {
    handleError(err, res);
  }
};


/*
=============================================================================
Udpdate  an existing Material
=============================================================================
*/
const opts = { runValidators: true, useFindAndModify: false , new : true};
exports.updateMaterial = async function(req, res) {
  try {
    if (!MaterialModel.isValidId(req.params.id)) {
      throw new Error("Supplied material id is invalid");
    }

    const materialToUpdate = await MaterialModel.findById(req.params.id).exec();
    if(materialToUpdate.number !== req.body.number){
      const existingMaterial = await MaterialModel.findOne({number: req.body.number}).exec();
      if(existingMaterial){
        res.status(400).json({ codeName: 'DuplicateKey' });
        return;
      }
    }

    let updatedItem = await MaterialModel.findByIdAndUpdate(
      req.params.id,
      { $set: req.body },
      opts
    ).exec();

    res.send({
      data: updatedItem,
      message: updatedItem
        ? `Material updated successfully`
        : `Material failed to updated`
    });
  } catch (err) {
    handleError(err, res);
  }
};

/*
 *api/materials: get all materials
 */
exports.findAll = async function(req, res) {
  try {
    let materials = null;

    materials = await MaterialModel.find().exec();

    res.send({
      data: materials,
      message: ``
    });
  } catch (err) {
    handleError(err, res);
  }
};

/*
 *api/materials/active: get all materials
 */
exports.findAllActiveMaterials = async function(req, res) {
  try {
    let materials = null;

    materials = await MaterialModel.find({inactive: false}).exec();

    res.send({
      data: materials,
      message: ``
    });
  } catch (err) {
    handleError(err, res);
  }
};

exports.handleError = (error, res) => {
  console.log(error.message);

  if (!error.status) {
    error.status = 400;
  }

  res.status(error.status).json({ codeName: error.codeName });
};

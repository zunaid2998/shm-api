const Email = require("email-templates");
const sgMail = require('@sendgrid/mail');
const moment = require("moment")
const fs = require("fs");

const emailTemplate = () => {
  return new Email({
    message: {
      from: "no-reply@envoyy.ca"
    },
    //We don't want it to open a browser preview.
    preview: false,
    send: true,
    transport: {
      host: "smtp.mailtrap.io",
      port: 2525,
      ssl: false,
      tls: true,
      auth: {
        user: process.env.MAILTRAP_USER, // Mailtrap username
        pass: process.env.MAILTRAP_PASS //Mailtrap password
      }
    }
  });
};

const prepareMaterialTable = (materials) => {
  let materialTable = ''
  materials.forEach(material => {
    materialTable = materialTable + `<tr><td>${material.number}</td>
                                      <td>${material.description}</td>
                                      <td>${material.quantity}</td>
                                      <td>${material.unitOfMeasure}</td></tr>`
  })
  return materialTable
}

const prepareMaterialTableWithAllocated = (materials) => {
  let materialTable = ''
  materials.forEach(material => {
    materialTable = materialTable + `<tr>
                                      <td>${material.number}</td>
                                      <td>${material.description}</td>
                                      <td>${material.unitOfMeasure}</td>
                                      <td>${material.quantity}</td>
                                      <td>${material.allocated}</td>
                                    </tr>`
  })
  return materialTable
}

const sendOrderCreationEmail = (sendOrderToEmail, order) => {
  const { email, materials, address, orderNo,
    dateRequired, orderPriority, facilityType, deliveryInstructions, customerName } = order;
  const materialRows = prepareMaterialTable(materials)
  const orderRequiredDate = moment(dateRequired).format('MMMM DD, YYYY')
  const delInstruction = deliveryInstructions || ``
  if (process.env.SENDGRID_API_KEY) {
    sgMail.setApiKey(process.env.SENDGRID_API_KEY);
    const msg = {
      to: sendOrderToEmail,
      from: `${process.env.FROM_EMAIL}`,
      subject: `SHM PPE Order #${orderNo} Confirmed`,
      html: `<h3>A new order has been posted recently from ${email} </h3>
             <p>ORDER #: ${orderNo}</p>
             <p></p>
             <p>
              Requester: ${customerName} <br>
              Email: ${email}
             </p>
             <p></p>
             <p>Date Required: ${orderRequiredDate}</p>
             <p></p>
             <p>Order Priority: ${orderPriority}</p>
             <p></p>
             <p>Facility Type: ${facilityType}</p>
             <p></p>
             <p></p>
             <p></p>
             <p>SHIPPING ADDRESS</p>
             <p></p>
             <p>${address.addressName} <br>
                ${address.addressLine1} <br>
                ${address.city}, ${address.province} <br>
                ${address.postalCode}</p>
             <p></p>
             <p></p>
             <p></p>
             <p></p>
             <p>Order Details </p>
             <table border="1" width="100%">
              <tr>
                <th>Material No.</th>
                <th>Description</th>
                <th>Quantity</th>
                <th>Unit Of Measure</th>
              </tr>
              ${materialRows}
             </table>
             <p></p>
             <p>Delivery Instructions: <br>
             ${delInstruction}
             </p>
              `
    };
    sgMail.send(msg)
      .then(() => console.log(`Email successfully sent`))
      .catch(err => console.error(`Unable to send email ${err}`));
  } else {
    console.log('Sendgrid API key missing.')
  }
}

const sendOrderCreationEmailToCustomer = (order) => {
  const { email, orderNo, customerName } = order;
  if (process.env.SENDGRID_API_KEY) {
    sgMail.setApiKey(process.env.SENDGRID_API_KEY);
    const msg = {
      to: email,
      from: `${process.env.FROM_EMAIL}`,
      subject: `SHM PPE Order #${orderNo} Confirmed`,
      html: `<h1>Hi ${customerName},</h1>
             <p>Thank you for your order!</p>
             <p>Your order has been received. 
             For future reference, your Order Number is ${orderNo}. If you have any questions, 
             please contact the SCM Incident Command Mailbox SCMCommandIntake@sharedhealthmb.ca</p>`
    };
    sgMail.send(msg)
      .then(() => console.log(`Email successfully sent`))
      .catch(err => console.error(`Unable to send email ${err}`));
  } else {
    console.log('Sendgrid API key missing.')
  }
}

const sendOrderProcessedEmailToCustomer = (order) => {
  const { email, materials, address, orderNo,
    dateRequired, orderPriority, facilityType, deliveryInstructions, customerName } = order;
  const materialRows = prepareMaterialTableWithAllocated(materials)
  const orderRequiredDate = moment(dateRequired).format('MMMM DD, YYYY')
  const delInstruction = deliveryInstructions || ``
  if (process.env.SENDGRID_API_KEY) {
    sgMail.setApiKey(process.env.SENDGRID_API_KEY);
    const msg = {
      to: email,
      from: `${process.env.FROM_EMAIL}`,
      subject: `SHM PPE Order #${orderNo} Processed`,
      html: `<h1>Hi ${customerName},</h1>
             <p>Your order has been processed.</p>
             <p></p>
             <p></p>
             <p>ORDER #: ${orderNo}</p>
             <p></p>
             <p>Date Required: ${orderRequiredDate}</p>
             <p></p>
             <p>Order Priority: ${orderPriority}</p>
             <p></p>
             <p>Facility Type: ${facilityType}</p>
             <p></p>
             <p></p>
             <p></p>
             <p>SHIPPING ADDRESS</p>
             <p></p>
             <p>${address.addressName} <br>
                ${address.addressLine1} <br>
                ${address.city}, ${address.province} <br>
                ${address.postalCode}</p>
             <p></p>
             <p></p>
             <p></p>
             <p></p>
             <p>Order Details </p>
             <table border="1" width="100%">
              <tr>
                <th>Material No.</th>
                <th>Description</th>
                <th>Unit Of Measure</th>
                <th>Quantity Ordered</th>
                <th>Quantity Allocated</th>
              </tr>
              ${materialRows}
             </table>
             <p></p>
             <p>Delivery Instructions: <br>
             ${delInstruction}
             </p>
             <p></p>
             <p></p>
             <p>Disclaimer - Your order has been allocated to our best ability. If you did not receive your full ordered quantity you will have to submit a new order, as we do not manage back orders</p>
             `
    };
    sgMail.send(msg)
      .then(() => console.log(`Email successfully sent`))
      .catch(err => console.error(`Unable to send email ${err}`));
  } else {
    console.log('Sendgrid API key missing.')
  }
}

const sendResetPasswordLink = (user) => {
  const { email, emailVerification } = user
  const baseUrl = process.env.ROOT_URL
  console.log(`Sending reset password email to: ${email}`);

  if (process.env.SENDGRID_API_KEY) {
    sgMail.setApiKey(process.env.SENDGRID_API_KEY);
    const msg = {
      to: email,
      from: `${process.env.FROM_EMAIL}`,
      subject: `SCM PPE Portal – Password Change`,
      html: `<p>Hello,</p>
              <p>You have either been set up as a new user on the SCM PPE Portal or have requested a password reset.</p>
              <p>To set your new password, please click on this link - 
              <a target="_blank" href="${baseUrl}/auth/reset-password/${emailVerification.code}">Reset your password</a></p>
              <p>Please ensure you create a unique password unrelated to any other work account.</p>
              <p>Thank you,</p>
              <p>If you have any questions about this email, please contact Shared Health SCM Command Intake scmcommandintake@sharedhealthmb.ca</p>
              <p></p>
              <p></p>
              <p><b>This link will expire in 24 hours.</b></p>`
    };
    sgMail.send(msg);
  } else {
    console.log("Missing sendgrid API key")
  }

};

const sendVerificationEmail = (email, person) => {
  console.log(`Sending verification email: ${email}`);

  if (process.env.SENDGRID_API_KEY) {
    const { firstName, verification_url, code } = person;
    sgMail.setApiKey(process.env.SENDGRID_API_KEY);
    const msg = {
      to: email,
      from: 'no-reply@shm.ca',
      subject: `${firstName}, Welcome to Envoyy!`,
      html: `<h1>Hi ${firstName},</h1>
              To verify your email, please click on the link and
              enter the code below.
            <h3><a target="_blank" href="${verification_url}/email-verification?code=${code}">Confirm my email</a></h3>
            <h3>Confirmation code ${code}</h3>`
    };
    sgMail.send(msg);
  } else {
    emailTemplate()
      .send({
        template: "verification",
        message: {
          to: email
        },
        locals: person // person is an object with first and last name
      })
      .then(() => console.log(`Email successfully sent`))
      .catch(err => console.error(`Unable to send email ${err}`));
  }

};

const sendNotificationEmail = (email, payload) => {
  console.log(`Sending verification email: ${email}`);

  if (process.env.SENDGRID_API_KEY) {
    const { recipientName, message_url, senderName } = payload;
    sgMail.setApiKey(process.env.SENDGRID_API_KEY);
    const msg = {
      to: email,
      from: 'no-reply@envoyy.ca',
      subject: `${recipientName}, You have a new message!`,
      html: `<h1>Hi ${recipientName},</h1>
              You have a new message from ${senderName}. 
              Visit ${message_url}, or click the link below. 
            <h4><a target="_blank" href="${message_url}"> here </a> My messages </h4>
            <br>
            <h5>*You must be logged in to view your messages.</h5>`
    };
    sgMail.send(msg);
  } else {
    emailTemplate()
      .send({
        template: "notification",
        message: {
          to: email
        },
        locals: payload
      })
      .then(() => console.log(`Email successfully sent`))
      .catch(err => console.error(`Unable to send email ${err}`));
  }

};

const sendDailyReport = (emails, fileName) => {
  pathToAttachment = `uploads/dailyreport/${fileName}.txt`;
  attachment = fs.readFileSync(pathToAttachment).toString("base64");

  if (process.env.SENDGRID_API_KEY) {
    sgMail.setApiKey(process.env.SENDGRID_API_KEY);
    const msg = {
      to: emails,
      from: `${process.env.FROM_EMAIL}`,
      subject: `SHM Daily Report`,
      html: `<h1>Hi,</h1>
              <p>Please find the attached daily report</p>`,
      attachments: [
        {
          content: attachment,
          filename: `${fileName}.txt`,
          type: "application/text",
          disposition: "attachment"
        }
      ]
    };
    sgMail.send(msg)
      .then(() => console.log(`Daily report email successfully sent`))
      .catch(err => console.error(`Unable to send email ${err}`));
  }
};


module.exports = {
  emailTemplate, sendVerificationEmail,
  sendNotificationEmail, sendOrderCreationEmail,
  sendOrderCreationEmailToCustomer, sendResetPasswordLink,
  sendOrderProcessedEmailToCustomer, sendDailyReport
};

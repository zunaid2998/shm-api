const generator = require("generate-password");

// --------------------------------------------------------------
// Random Unique Digit Generator --------------------------------
// --------------------------------------------------------------

const UniqueDigitGenerator = () => {
  return generator.generate({
    length: 6,
    exclude: "O0abcdefghijklmnopqrstuvwxyz",
    uppercase: false,
    numbers: true
  });
};

module.exports = UniqueDigitGenerator;

const mongoose = require("mongoose");

exports.USER_TYPE = {
  client: "client",
  serviceProvider: "service_provider"
};

exports.isValidId = id => {
  try {
    return mongoose.Types.ObjectId.isValid(id);
  } catch (err) {
    console.log(`[isValidId] : ${err}`);
  }
  return false;
};

const utils = {
  generateTestEmail: function() {
    let gen = "abcdefghijklmnopqrstuvqxyzABCDEFGHIJKLMNOPQRSTUVWXYZ123456789";
    let idx = Math.floor(Math.random() * gen.length);
    let idx2 = Math.floor(Math.random() * gen.length);
    return `test${gen[idx]}${gen[idx2]}@test.ca`;
  }
};
module.exports = utils;

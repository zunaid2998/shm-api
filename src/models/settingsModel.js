const mongoose = require("mongoose");

const settingsSchema = mongoose.Schema({
  sendOrderToEmail: {
    type: String
  },

  facilityTypes: [{
    type: String
  }]
}, {
  timestamps: true
});

/*
=============================================================================
Check that it is valid according to Mongoose convention
=============================================================================
*/
settingsSchema.statics.isValidId = id => {
  try {
    console.log(`Checking id : ${id}`);
    return mongoose.Types.ObjectId.isValid(id);
  } catch (err) {
    console.log(`[isValidId] : ${err}`);
  }
  return false;
};

SettingsModel = mongoose.model("settings", settingsSchema);

module.exports = SettingsModel;

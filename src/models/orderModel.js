const mongoose = require("mongoose");

const orderSchema = mongoose.Schema({
  customerId: mongoose.ObjectId,
  customerName: {
    type: String,
    required: true
  },

  address: {
    addressLine1: {type: String},
    city: {type: String},
    province: {type: String},
    postalCode: {type: String},
    addressName: {type: String}
  },

  email: {
    type: String,
    required: true
  },

  materials: {
    type: Array,
    default: []
  },

  orderNo: {
    type: Number,
    unique: true,
    required: true
  },

  status: {
    type: String
  },

  dateRequired: {
    type: Date
  },

  orderPriority: {
    type: String
  },

  facilityType: {
    type: String
  },

  deliveryInstructions: {
    type: String
  },

  extracted: {
    type: Boolean
  }
}, {
  timestamps: true
});

/*
=============================================================================
Check that it is valid according to Mongoose convention
=============================================================================
*/
orderSchema.statics.isValidId = id => {
  try {
    console.log(`Checking id : ${id}`);
    return mongoose.Types.ObjectId.isValid(id);
  } catch (err) {
    console.log(`[isValidId] : ${err}`);
  }
  return false;
};

OrderModel = mongoose.model("order", orderSchema);

module.exports = OrderModel;

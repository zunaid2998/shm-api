const mongoose = require("mongoose");

const userSchema = mongoose.Schema(
  {
    firstName: {
      type: String,
      trim: true,
      required: true
    },
    lastName: {
      type: String,
      trim: true,
      required: true
    },
    email: {
      type: String,
      required: [true, "Email is required."],
      unique: true,
      trim: true,
      lowercase: true,
      validate: {
        validator: function(value) {
          // email validation
          const regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
          return regex.test(value);
        },
        message: props =>
          `"${props.path}" must be a valid email. However we received ${props.value}`
      }
    },
    password: {
      type: String,
      trim: true
    },
    userType: {
      type: String,
      required: true,
      lowercase: true
    },
    address: [{
      addressLine1: {type: String},
      city: {type: String},
      province: {type: String},
      postalCode: {type: String},
      addressName: {type: String}
    }],
    suspended: {
      type: Boolean,
      default: false
    },
    createdAt: {
      type: Date,
      default: Date.now()
    },
    emailVerification: {
      code: {type: String},
      expiredAt: {type: Date}
    },
    apiToken : {
      type: String
    }
  },
);

/*
=============================================================================
Check that it is valid according to Mongoose convention
=============================================================================
*/
userSchema.statics.isValidId = id => {
  try {
    return mongoose.Types.ObjectId.isValid(id);
  } catch (err) {
    console.log(`[isValidId] : ${err}`);
  }
  return false;
};

/*
=============================================================================
Modify returning user
=============================================================================
*/

userSchema.methods.toJSON = function() {
  const user = this;
  const userObject = user.toObject();

  delete userObject.password;

  return userObject;
};


UserModel = mongoose.model("user", userSchema);

module.exports = UserModel;

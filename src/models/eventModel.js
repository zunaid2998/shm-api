const mongoose = require("mongoose");

const eventSchema = mongoose.Schema({
  userId: mongoose.ObjectId,

  userName: {
    type: String,
    required: true
  },

  event: {
    type: String,
    required: true
  }
}, {
  timestamps: true
});

/*
=============================================================================
Check that it is valid according to Mongoose convention
=============================================================================
*/
eventSchema.statics.isValidId = id => {
  try {
    console.log(`Checking id : ${id}`);
    return mongoose.Types.ObjectId.isValid(id);
  } catch (err) {
    console.log(`[isValidId] : ${err}`);
  }
  return false;
};

EventModel = mongoose.model("event", eventSchema);

module.exports = EventModel;

const mongoose = require("mongoose");

const materialSchema = mongoose.Schema({
  number: {
    type: String,
    required: true,
    unique: true
  },

  description: {
    type: String,
    required: true
  },

  unitOfMeasure: {
    type: String,
  },

  createdAt: {
    type: Date,
    default: Date.now()
  },

  inactive: {
    type: Boolean,
    default: false
  }
});

/*
=============================================================================
Check that it is valid according to Mongoose convention
=============================================================================
*/
materialSchema.statics.isValidId = id => {
  try {
    console.log(`Checking id : ${id}`);
    return mongoose.Types.ObjectId.isValid(id);
  } catch (err) {
    console.log(`[isValidId] : ${err}`);
  }
  return false;
};

MaterialModel = mongoose.model("material", materialSchema);

module.exports = MaterialModel;

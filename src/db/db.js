const mongoose = require("mongoose");
const fs = require("fs");
const ca = fs.readFileSync(__dirname + "/rds-combined-ca-bundle.pem");

mongoose.connect(process.env.MONGODB_URL, {
  // useCreateIndex assures us that when mongoose connect to mongodb,
  // the indexes are created
  useNewUrlParser: true,
  useCreateIndex: true,
  useFindAndModify: true,
  useUnifiedTopology: true,
  sslCA: ca
});

const db = mongoose.connection;

db.on("connected", function() {
  console.log("connected to database");
});

db.on("error", function(err) {
  console.log(`DB: error ${err}`);
});

db.on("disconnected", function() {});

process.on("SIGINT", function() {
  try {
    db.close(function() {
      console.log("Killing connection");
      process.exit(0);
    });
  } catch (error) {
    console.log("Error closing the connection ...");
  }
});
